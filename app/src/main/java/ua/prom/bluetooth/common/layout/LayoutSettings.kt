package ua.prom.bluetooth.common.layout

import android.support.annotation.LayoutRes
import android.support.annotation.StringRes

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class LayoutSettings(
    @LayoutRes val layoutId: Int = 0,
    @StringRes val title: Int = 0,
    val setWithoutBinding: Boolean = false,
    val hideToolbar: Boolean = false
)