package ua.prom.bluetooth.view.scan

import android.databinding.DataBindingUtil
import android.databinding.ObservableBoolean
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_scan.*
import ua.prom.bluetooth.BR
import ua.prom.bluetooth.R
import ua.prom.bluetooth.common.layout.LayoutSettings
import ua.prom.bluetooth.databinding.FragmentScanBinding
import ua.prom.bluetooth.ext.replaceBackStack
import ua.prom.bluetooth.view.RouterState
import ua.prom.bluetooth.view.main.MainActivity
import ua.prom.bluetooth.view.training.TrainingFragment

@LayoutSettings(R.layout.fragment_scan)
class ScanFragment : Fragment() {

    lateinit var binding: FragmentScanBinding
    val isDeviceFound = ObservableBoolean(false)
    val isDeviceNotFound = ObservableBoolean(false)
    val isScanning = ObservableBoolean(false)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_scan, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.setVariable(BR.isDeviceFound, isDeviceFound)
        binding.setVariable(BR.isDeviceNotFound, isDeviceNotFound)
        binding.setVariable(BR.isScanning, isScanning)

        btnConnect.setOnClickListener {
            fragmentManager?.replaceBackStack(RouterState.TRAINING.name, R.id.containerMain) { TrainingFragment() }
        }
        btnRepeat.setOnClickListener {
            (activity as MainActivity).scan()
            isDeviceFound.set(false)
            isDeviceNotFound.set(false)
        }

        (activity as MainActivity).onDeviceFound = {
            isDeviceFound.set(true)
            isScanning.set(false)
        }
        (activity as MainActivity).onDeviceNotFound = {
            isDeviceNotFound.set(true)
            isScanning.set(false)
        }
        (activity as MainActivity).onScanStarted = { isScanning.set(true) }
    }

    override fun onResume() {
        super.onResume()
        binding.executePendingBindings()
    }

}
