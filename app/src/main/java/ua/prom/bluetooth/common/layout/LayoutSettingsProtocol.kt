package ua.prom.bluetooth.common.layout


interface LayoutSettingsProtocol {

    fun getLayoutTitle(): Int = getAnnotation().title

    fun getLayoutId(): Int = getAnnotation().layoutId

    fun isSetAsContent(): Boolean = getAnnotation().setWithoutBinding

    fun hideToolbar(): Boolean = getAnnotation().hideToolbar

    private fun getAnnotation(): LayoutSettings =
        javaClass.getAnnotation(LayoutSettings::class.java)
            ?: throw RuntimeException("Cannot find LayoutSettings annotation")
}