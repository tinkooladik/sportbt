package ua.prom.bluetooth.view.main

import android.databinding.ViewDataBinding
import android.view.View
import android.view.ViewGroup
import ua.prom.bluetooth.BR
import ua.prom.bluetooth.R
import ua.prom.bluetooth.common.adapter.BaseAdapter
import ua.prom.bluetooth.common.adapter.BaseBindingViewHolder
import ua.prom.bluetooth.ext.inflate

class DeviceAdapter : BaseAdapter<DeviceVM, DeviceAdapter.DeviceItemViewHolder>() {

    var onDisconnectClick: ((DeviceVM) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceItemViewHolder =
        DeviceItemViewHolder(parent.inflate(R.layout.item_device))

    inner class DeviceItemViewHolder(view: View) :
        BaseBindingViewHolder<DeviceVM>(view, BR.deviceVM) {

        override fun addVariable(binding: ViewDataBinding?) {
            binding?.setVariable(BR.deviceAH, DeviceItemActionHolder(onDisconnectClick))
        }
    }
}

class DeviceItemActionHolder(
    private val disconnectClickListener: ((DeviceVM) -> Unit)? = null
) {

    fun onDisconnectClick(item: DeviceVM) {
        disconnectClickListener?.invoke(item)
    }
}