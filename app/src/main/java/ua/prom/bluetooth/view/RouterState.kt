package ua.prom.bluetooth.view


enum class RouterState {
    SCAN,
    TRAINING,
    RESULT,
    GO_BACK
}