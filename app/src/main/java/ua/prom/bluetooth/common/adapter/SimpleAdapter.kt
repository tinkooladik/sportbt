package ua.prom.bluetooth.common.adapter

import android.databinding.BaseObservable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class SimpleAdapter<VM : BaseObservable>(private val holderLayout: Int, private val variableId: Int) :
    BaseAdapter<VM, SimpleAdapter<VM>.SimpleBindingHolder<VM>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleBindingHolder<VM> {
        val v = LayoutInflater.from(parent.context).inflate(holderLayout, parent, false)
        return SimpleBindingHolder(v)
    }

    inner class SimpleBindingHolder<VM : BaseObservable>(v: View) : BaseBindingViewHolder<VM>(v, variableId)
}