package ua.prom.bluetooth.view.main

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.polidea.rxandroidble2.LogConstants
import com.polidea.rxandroidble2.LogOptions
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.RxBleClient.State.*
import com.polidea.rxandroidble2.scan.ScanSettings
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import timber.log.Timber
import ua.prom.bluetooth.R
import ua.prom.bluetooth.ext.initWithAdapter
import ua.prom.bluetooth.ext.replace
import ua.prom.bluetooth.view.RouterState
import ua.prom.bluetooth.view.scan.ScanFragment
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit.SECONDS

private const val REQUEST_ENABLE_BT = 1
private val UUID_BLUETOOTH_LE_CC254X_CHAR_RW = UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb")
private val UUID_BLUETOOTH_LE_BATTERY_LEVEL = UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb")

private const val CSV_HEADER = "n,hl,hr,p1,p2,p3,p4,p5,p6,p7"

class MainActivity : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()
    private var connection: Disposable? = null
    private val devices = mutableListOf<DeviceVM>()
    private val adapter = DeviceAdapter()
    private lateinit var rxBleClient: RxBleClient

    private val characteristicText = StringBuilder()
    private var mac: String? = null

    var timeStart: Long? = null
    var duration: Long? = null

    var onDeviceFound: (() -> Unit)? = null
    var onDeviceNotFound: (() -> Unit)? = null
    var onScanStarted: (() -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvDevices.initWithAdapter(adapter)
        adapter.items = devices
        adapter.onItemClickListener = { item ->
            if (item.mac != null) {
                mac = item.mac
                connect()
            } else toast("Cannot connect to null MAC address")
        }
        adapter.onDisconnectClick = { disconnect() }

        rxBleClient = RxBleClient.create(this)
        RxBleClient.updateLogOptions(
            LogOptions.Builder()
                .setLogLevel(LogConstants.VERBOSE)
                .setUuidsLogSetting(LogConstants.UUIDS_FULL)
                .build()
        )

        val stateSub = rxBleClient.observeStateChanges()
            .startWith(rxBleClient.state)
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it) {
                    READY -> checkPermission()
                    BLUETOOTH_NOT_AVAILABLE -> toast("Device doesn't support Bluetooth")
                    LOCATION_PERMISSION_NOT_GRANTED -> checkPermission()
                    BLUETOOTH_NOT_ENABLED -> showTurnBtOnDialog()
                    LOCATION_SERVICES_NOT_ENABLED -> toast("Device doesn't support location services, can't  performscan")
                    else -> toast("Unknown Bluetooth state")
                }
            },
                { handleError(it) }
            )
        compositeDisposable.add(stateSub)

        btnRefresh.setOnClickListener { checkPermission() }

        supportFragmentManager?.replace(RouterState.SCAN.name, R.id.containerMain) { ScanFragment() }
    }

    private fun showTurnBtOnDialog() {
        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
    }

    private fun checkPermission() {
        compositeDisposable.add(
            RxPermissions(this)
                .request(
                    Manifest.permission.BLUETOOTH,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
                .subscribe { granted ->
                    if (granted) scan() else toast("To use this application, you need to accept the permission")
                })
    }

    fun scan() {
        onScanStarted?.invoke()
        toast("scanning...")
        pbScan.visibility = View.VISIBLE
        btnRefresh.visibility = View.GONE
        devices.clear()
        val scanSubscription = rxBleClient
            .scanBleDevices(ScanSettings.Builder().build())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ scan ->
                if (devices.none { it.mac == scan.bleDevice.macAddress }) {
                    devices.add(DeviceVM().apply {
                        name = scan.bleDevice.name
                        mac = scan.bleDevice.macAddress
                        state.set(scan.bleDevice.connectionState.name)
                    })
                }
                if (scan.bleDevice.name == "HMSoft") {
                    mac = scan.bleDevice.macAddress
                    onDeviceFound?.invoke()
                }
            }, { handleError(it) }
            )
        Single.timer(5, SECONDS)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                pbScan.visibility = View.GONE
                btnRefresh.visibility = View.VISIBLE
                scanSubscription.dispose()
                if (mac.isNullOrEmpty()) onDeviceNotFound?.invoke()
            }, { Timber.e(it) })
            .let { compositeDisposable.add(it) }
    }

    fun connect() {
        toast("connecting to $mac...")
        mac?.let { mac ->
            val device = rxBleClient.getBleDevice(mac)
            timeStart = System.currentTimeMillis()

            device.observeConnectionStateChanges()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ devices.first { it.mac == mac }.updateState(it) },
                    { handleError(it) }
                ).let { compositeDisposable.add(it) }

            connection = device.establishConnection(false)
//            .flatMap {
//                Observable.combineLatest(
//                    it.setupNotification(UUID_BLUETOOTH_LE_CC254X_CHAR_RW).flatMap { it },
//                    it.setupNotification(UUID_BLUETOOTH_LE_BATTERY_LEVEL, NotificationSetupMode.COMPAT).flatMap { it },
//                    BiFunction<ByteArray, ByteArray, Observable<Pair<ByteArray, ByteArray>>>
//                    { t1, t2 -> Observable.just(Pair(t1, t2)) }
//                )
//            }
                .flatMap {
                    it.setupNotification(UUID_BLUETOOTH_LE_CC254X_CHAR_RW)
                }
                .flatMap { it }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    characteristicText.append(it.toString(Charsets.UTF_8))
                    tvLog.append(it.toString(Charsets.UTF_8))
//                    pair ->
//                tvLog.append(pair.first.toString(Charsets.UTF_8))
//                devices.first { it.mac == mac }.battery.set(pair.second.toString(Charsets.UTF_8))
                },
                    { handleError(it) })
            connection?.let { compositeDisposable.add(it) }
        }
    }

    fun disconnect() {
        connection?.dispose()
        writeCSV(characteristicText.toString())
    }

    private fun writeCSV(source: String) {
        val results = source.split("\n").filter { it.length > 20 }

        val folder = File(Environment.getExternalStorageDirectory().absolutePath + "/sportBt")
        if (!folder.exists()) folder.mkdir()

        val filename = "$folder/scan_results.csv"
        val file = File(filename)
        if (!file.exists()) file.createNewFile() else file.setLastModified(System.currentTimeMillis())

        var fileWriter: FileWriter? = null

        try {
            fileWriter = FileWriter(filename)

            fileWriter.append(CSV_HEADER)
            fileWriter.append('\n')

            results.forEachIndexed { pos, res ->
                val values = res.split("/").map { it.substringAfter(":") }.joinToString()
                fileWriter.append("$pos,$values")
                fileWriter.append('\n')
            }

            longToast("CSV saved! path: $filename")
        } catch (e: Exception) {
            toast("Writing CSV error!!")
            handleError(e)
        } finally {
            try {
                fileWriter?.flush()
                fileWriter?.close()
            } catch (e: IOException) {
                toast("Flushing/closing error!")
                Timber.e(e)
            }
        }
    }

    private fun handleError(e: Throwable) {
        Timber.e(e)
        toast(e.message ?: "Error")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_ENABLE_BT -> handleEnableBtResult(resultCode)
        }
    }

    private fun handleEnableBtResult(resultCode: Int) {
        when (resultCode) {
            Activity.RESULT_OK -> checkPermission()
            else -> toast("To use this application, you need to turn Bluetooth on")
        }
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        connection?.dispose()
        super.onDestroy()
    }
}
