package ua.prom.bluetooth.common.adapter

import android.databinding.BaseObservable
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.View

abstract class BaseBindingViewHolder<VM : BaseObservable>(view: View, varId: Int) :
    RecyclerView.ViewHolder(view) {

    var binding: ViewDataBinding? = null
    private val bindingVarID = varId

    init {
        bind()
    }

    fun unbind() {
        binding?.unbind()
    }

    fun setViewModel(vm: VM) {
        binding?.setVariable(bindingVarID, vm)
        addVariable(binding)
    }

    open fun addVariable(binding: ViewDataBinding?) {}

    fun bind() {
        binding = DataBindingUtil.bind(itemView)
    }
}