package ua.prom.bluetooth.view.training

import android.databinding.DataBindingUtil
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_training.*
import timber.log.Timber
import ua.prom.bluetooth.BR
import ua.prom.bluetooth.R
import ua.prom.bluetooth.common.layout.LayoutSettings
import ua.prom.bluetooth.databinding.FragmentTrainingBinding
import ua.prom.bluetooth.ext.replaceBackStack
import ua.prom.bluetooth.view.RouterState
import ua.prom.bluetooth.view.main.MainActivity
import ua.prom.bluetooth.view.result.ResultFragment
import java.util.concurrent.TimeUnit

@LayoutSettings(R.layout.fragment_training)
class TrainingFragment : Fragment() {

    //    companion object {
//        fun newInstance() = TrainingFragment()
//    }
//
//    private lateinit var viewModel: TrainingViewModel

    private val compositeDisposable = CompositeDisposable()

    lateinit var binding: FragmentTrainingBinding
    val isTrainingStarted = ObservableBoolean(false)
    val timer = TrainingTimer()
    var timerSub: Disposable? = null

    val left = ObservableInt(0)
    val right = ObservableInt(0)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_training, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.setVariable(BR.isTrainingStarted, isTrainingStarted)
        binding.setVariable(BR.timer, timer)
        binding.setVariable(BR.left, left)
        binding.setVariable(BR.right, right)

        btnStart.setOnClickListener {
            isTrainingStarted.set(true)
            startTimer()
            (activity as MainActivity).connect()
        }
        btnStop.setOnClickListener { endTraining() }
        btnEnd.setOnClickListener { endTraining() }
    }

    fun endTraining() {
        isTrainingStarted.set(false)
        timerSub?.dispose()
        (activity as MainActivity).disconnect()
        (activity as MainActivity).duration = timer.timer.get()?.toLong()
        fragmentManager?.replaceBackStack(RouterState.RESULT.name, R.id.containerMain) { ResultFragment() }
    }

    fun startTimer() {
        timerSub = Observable.interval(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                timer.timer.set(it.toString())
            }, { Timber.e(it) })
        timerSub?.let { compositeDisposable.add(it) }
    }

    override fun onResume() {
        super.onResume()
        binding.executePendingBindings()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    //    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProviders.of(this).get(TrainingViewModel::class.java)
//    }

}

class TrainingTimer {
    val timer = ObservableField("0")
}
