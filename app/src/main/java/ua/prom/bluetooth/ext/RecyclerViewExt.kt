package ua.prom.bluetooth.ext

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout

fun RecyclerView.initWithAdapter(
  adapter: RecyclerView.Adapter<*>, layoutManager: LinearLayoutManager? = null,
  @DrawableRes dividerRes: Int? = null
) {

  setLayoutManager(layoutManager ?: LinearLayoutManager(context))
  this.adapter = adapter

  if (dividerRes != null) {
    addItemDecoration(getDivider(context, dividerRes, layoutManager))
  }
}

private fun getDivider(
  context: Context, @DrawableRes dividerRes: Int,
  layoutManager: LinearLayoutManager? = null
): DividerItemDecoration {
  val divider = DividerItemDecoration(context, layoutManager?.orientation ?: LinearLayout.VERTICAL)
  divider.setDrawable(ContextCompat.getDrawable(context, dividerRes) ?: BitmapDrawable())
  return divider
}