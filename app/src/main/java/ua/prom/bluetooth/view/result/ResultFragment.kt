package ua.prom.bluetooth.view.result

import android.annotation.SuppressLint
import android.databinding.DataBindingUtil
import android.databinding.ObservableField
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_result.*
import ua.prom.bluetooth.BR
import ua.prom.bluetooth.common.layout.LayoutSettings
import ua.prom.bluetooth.databinding.FragmentResultBinding
import ua.prom.bluetooth.view.main.MainActivity
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


@LayoutSettings(ua.prom.bluetooth.R.layout.fragment_result)
class ResultFragment : Fragment() {

    lateinit var binding: FragmentResultBinding

    val results = TrainingResult()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, ua.prom.bluetooth.R.layout.fragment_result, container, false)
        return binding.root
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.setVariable(BR.results, results)

        btnSave.setOnClickListener { }

        (activity as MainActivity).timeStart?.let {
            tvDate.text =
                when {
                    isToday(it) -> "Today " + formatTime("HH:mm")
                    isYesterday(it) -> "Yesterday " + formatTime("HH:mm")
                    else -> formatTime("dd/MM/yyyy HH:mm")
                }
        }
        (activity as MainActivity).duration?.let {
            tvDuration.text =
                String.format(
                    "%d.%02d hours",
                    TimeUnit.SECONDS.toHours(it),
                    TimeUnit.SECONDS.toMinutes(
                        it - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(it))
                    )
                )
        }
    }

    private fun isYesterday(millis: Long) = DateUtils.isToday(Date(millis).time + DateUtils.DAY_IN_MILLIS)

    private fun isToday(millis: Long) = DateUtils.isToday(Date(millis).time)

    @SuppressLint("SimpleDateFormat") fun formatTime(pattern: String): String =
        SimpleDateFormat(pattern).format((activity as MainActivity).timeStart)

    override fun onResume() {
        super.onResume()
        binding.executePendingBindings()
    }
}

class TrainingResult {
    val leftDirect = ObservableField("0")
    val leftSide = ObservableField("0")
    val leftBottom = ObservableField("0")
    val leftTotal = ObservableField("0")
    val rightDirect = ObservableField("0")
    val rightSide = ObservableField("0")
    val rightBottom = ObservableField("0")
    val rightTotal = ObservableField("0")
}
