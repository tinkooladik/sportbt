package ua.prom.bluetooth.view.main

import android.databinding.BaseObservable
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import com.polidea.rxandroidble2.RxBleConnection.RxBleConnectionState

class DeviceVM : BaseObservable() {
    var name: String? = null
    var mac: String? = null
    var connected = ObservableBoolean()
    var state = ObservableField<String?>()
    var battery = ObservableField<String?>()

    fun updateState(newState: RxBleConnectionState) {
        state.set(newState.name)
        connected.set(newState.name == RxBleConnectionState.CONNECTED.name)
    }
}