package ua.prom.bluetooth

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

class BtApp : Application(), HasActivityInjector {

//  private val component: AppComponent by lazy {
//    DaggerAppComponent.builder()
//        .context(this)
//        .build()
//  }

  override fun onCreate() {
    super.onCreate()
//    component.inject(this)
    if (BuildConfig.DEBUG) {
      Timber.plant(Timber.DebugTree())
    }
  }

  @Inject
  lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

  override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector

}